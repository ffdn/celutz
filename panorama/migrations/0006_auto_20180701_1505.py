# -*- coding: utf-8 -*-
# Generated by Django 1.9.13 on 2018-07-01 15:05
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('panorama', '0005_auto_20170912_2147'),
    ]

    operations = [
        migrations.RenameField(
            model_name='referencepoint',
            old_name='altitude',
            new_name='ground_altitude',
        ),
        migrations.AlterField(
            model_name='referencepoint',
            name='ground_altitude',
            field=models.FloatField(help_text='In meters', validators=[django.core.validators.MinValueValidator(0.0)], verbose_name='altitude at ground level'),
        ),
        migrations.AddField(
            model_name='referencepoint',
            name='height_above_ground',
            field=models.FloatField(default=0.0, help_text='In meters', verbose_name='height above ground'),
        ),
        migrations.AlterUniqueTogether(
            name='reference',
            unique_together=set([('reference_point', 'panorama')]),
        ),
    ]
